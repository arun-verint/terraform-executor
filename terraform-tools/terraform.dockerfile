FROM hashicorp/terraform:0.12.30

# Build parameters
ARG KUBECTL_LATEST_VERSION=v1.13.4
ENV KLV=${KUBECTL_LATEST_VERSION}

RUN apk add --no-cache --update curl gettext python3 jq py-pip openssl ca-certificates graphviz ttf-freefont bash \
    # Installing Kubernetes client
    && curl -L https://storage.googleapis.com/kubernetes-release/release/v1.13.4/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl \
    # Installing GCloud CLI
    && curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-337.0.0-linux-x86_64.tar.gz \
    && tar zxf google-cloud-sdk-337.0.0-linux-x86_64.tar.gz \
    && export CLOUDSDK_CORE_DISABLE_PROMPTS=1 \
    && ./google-cloud-sdk/install.sh \
    && export PATH=/google-cloud-sdk/bin:$PATH \
    && gcloud components update \
    && rm -rf /var/cache/apk/*

ENTRYPOINT ["terraform","-version"]
